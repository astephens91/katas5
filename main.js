//=============================================
//              Reverse String
//=============================================

function reverseString(string){
    return string.split("").reverse().join("");
};
// reverseString Tests
//First one test a one word string
function testReverseString1(){
    let result = reverseString("Foxbox")
    console.assert(result === "xobxoF", {
        "function": "reverseString('Foxbox')",
        "expected": 'xobxoF',
        "result": result
    });
};
//Second one test a multi-word string
function testReverseString2(){
    let result = reverseString("well achtually mam")
    console.assert(result === "mam yllauthca llew", {
        "function": "reverseString('well achtually mam')",
        "expected": "mam yllauthca llew",
        "result": result
    });
};
testReverseString1();
testReverseString2();

//=============================================
//              Reverse Sentence
//=============================================

function reverseSentence(sentence){
    return sentence.split("").reverse().join("");
};
//reverseSentence Tests
//First one uses all lowercase
function testReverseSentence1(){
    let result = reverseSentence("i was sad but now i am glad because of mario.")
    console.assert(result === ".oiram fo esuaceb dalg ma i won tub das saw i", {
        "function": "reverseString('i was sad but now i am glad because of mario.')",
        "expected": '.oiram fo esuaceb dalg ma i won tub das saw i',
        "result": result
    });
};
//Second one tests sentence with upper and lower case letters and commas
function testReverseSentence2(){
    let result = reverseSentence("I was glad, but then Luigi made me sad again.")
    console.assert(result === ".niaga das em edam igiuL neht tub ,dalg saw I", {
        "function": "reverseString('well achtually mam')",
        "expected": ".niaga das em edam igiuL neht tub ,dalg saw I",
        "result": result
    });
};

testReverseSentence1();
testReverseSentence2();

//=============================================
//              Minimum Value
//=============================================

function minimumValue(value){
    if (value.length > 0){
    return Math.min(...value)
    }
    else{
        return "No Numbas!"
    }
};

//MinimumValue Tests
//First one tests for smalles value in array
function testMinimumValue1(){
    let result = minimumValue([663246,675467,2465246,8658765,14352345,87654865,245643,2345,7654783,1452])
    console.assert(result === 1452, {
        "function": "minimumValue([663246,675467,2465246,8658765,14352345,87654865,245643,2345,7654783,1452])",
        "expected": 1452,
        "result": result
    });
};

//Second one tests for empty array
function testMinimumValue2(){
    let result = minimumValue([])
    console.assert(result === "No Numbas!", {
        "function": "minimumValue([])",
        "expected": "No Numbas!",
        "result": result
    });
};
testMinimumValue1();
testMinimumValue2();

//=============================================
//              Maximum Value
//=============================================

function maximumValue(value){
    if (value.length > 0){
        return Math.max(...value)
        }
        else{
            return "No Numbas!"
        }
};

//MaximumValue Tests
//First one tests for largest value in array
function testMaximumValue1(){
    let result = maximumValue([663246,675467,2465246,8658765,14352345,87654865,245643,2345,7654783,1452])
    console.assert(result === 87654865, {
        "function": "maximumValue([663246,675467,2465246,8658765,14352345,87654865,245643,2345,7654783,1452])",
        "expected": 87654865,
        "result": result
    });
};

//Second one tests for empty array
function testMaximumValue2(){
    let result = maximumValue([])
    console.assert(result === "No Numbas!", {
        "function": "maximumValue([])",
        "expected": "No Numbas!",
        "result": result
    });
};
testMaximumValue1();
testMaximumValue2();

//=============================================
//            Calculate Remainder
//=============================================

function calculateRemainder(x, y){
    if (y !== 0){
        return y % x
    }
    else {
        return "Singularity Created"
    }
}

//CalculateRemainder Tests
//First one tests for accurate remainder
function testCalculateRemainder1(){
    let result = calculateRemainder(4, 2)
    console.assert(result === 2, {
        "function": "calculateRemainder(4, 2)",
        "expected": 2,
        "result": result
    })
}

//Second one tests for attempt to divide by zero
function testCalculateRemainder2(){
    let result = calculateRemainder(4, 0);
    console.assert(result === "Singularity Created", {
        "function": "calculateRemainder(4, 0)",
        "expected": "Singularity Created",
        "result": result
    })
}
testCalculateRemainder1()
testCalculateRemainder2()

//=============================================
//              Distinct Values
//=============================================

function distinctValues(values){
    
    if (values != undefined){
        let array = values.split(",")
        let result = [];
        array.forEach(function(element){
            if (!result.includes(element)){
                result.push(element)
            }
        })
        return result.join()
    }
    else{
        return "No Numbas!"
    }
}

//DistinctValues Tests
//First one tests for actual distinct values

function testDistinctValues1(){
    let result = distinctValues("1,4,4,4,1,3,6,4");
    console.assert(result == "1,4,3,6", {
        "function": "distinctValues([1,4,4,4,1,3,6,4]",
        "expected": "1,4,3,6",
        "result": result
    });
}

//Second one tests for empty array input

function testDistinctValues2(){
    let result = distinctValues();
    console.assert(result === "No Numbas!", {
        "function": "distinctValues([])",
        "expected": "No Numbas!",
        "result": result
    })
}
testDistinctValues1();
testDistinctValues2();

//=============================================
//              Count Values
//=============================================

function countValues(values){
    if (values != undefined){
        let array = values.split(",");
        let result = [];
        
    }
}

//Count Values Tests
//First one simply checks for count values

function testCountValues1(){
    let result = countValues("1,4,4,4,1,4,5,6,7,8,8")
    console.assert(result === "1(2),4(4),5(1),6(1),7(1),8(2)", {
        "function": "countValues('1,4,4,4,1,4,5,6,7,8,8')",
        "expected": "1(2),4(4),5(1),6(1),7(1),8(2)",
        "result": result
    })
}
testCountValues1()